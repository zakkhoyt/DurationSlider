# DurationSliderKit
A date/time/duration picker which is similar to the Bedtime tab on iOS's Clock app.  

# Preview

![Demo Video 1](https://giant.gfycat.com/AcrobaticPleasantHusky.webm)
![Demo Video 2](https://giant.gfycat.com/GregariousHeartyGermanspaniel.webm)
![Demo Video 3](https://giant.gfycat.com/CraftyBabyishBangeltiger.webm)

Full video demo [here](https://youtu.be/buHi-wYj5nE)

# Features

* Pick a duration by selecting start date and end date.
* Set a minimum duration between startDate and endDate (e.g. 30 minutes).
* Set a maximum duration between startDate and endDate (e.g. 18 hours).
* Set an absolute minimum date (similar to UIDatePicker)
* Set an absolute maximum date (similar to UIDatePicker)
* Displays the duration as a formatted string, but retuns a TimeInterval.
* Fully configurable colors and fonts.
* Choose to dispay text, image, color, or nothing inside the knobs.
* Snap to the nearest "n" seconds (optional).
* Provides haptic feedback when snapping is enabled


# Configuration

````
open var startDate: Date 
open var endDate: Date 
open var minimumDate: Date?
open var maximumDate: Date?
open var duration: TimeInterval 
open var maximumDuration: TimeInterval? 
open var minimumDuration: TimeInterval? 
open var snapToSeconds: TimeInterval? 
open var startKnobText: String?
open var endKnobText: String?
open var startKnobImage: UIImage? 
open var endKnobImage: UIImage? 
open var startKnobColor: UIColor 
open var endKnobColor: UIColor 
open var trackColor: UIColor 
open var knobWidth: CGFloat 
open override var tintColor: UIColor! 
open var durationTextAttributes: [NSAttributedStringKey: Any] 

// MARK: Clock stuff
open var tickInset: CGFloat
open var majorTickLength: CGFloat
open var majorTickWidth: CGFloat
open var minorTickLength: CGFloat
open var minorTickWidth: CGFloat
open var clockTextAttributes: [NSAttributedStringKey: Any] 
````

# Requirements

* Minimum required iOS version 9.0

# Demo 

A demo application is available along with DurationSliderKit

````
git clone https://gitlab.com/zakkhoyt/DurationSlider
````
Open DurationSliderDemo.xcodeproj
Select DurationSliderDemo scheme 
Run

# Cocoapods

Add this to your Podfile:

````
  platform :ios, '9.0'
  use_frameworks!
  pod 'DurationSliderKit', :git => 'https://gitlab.com/zakkhoyt/DurationSlider', :branch => '1.0.0'
````

Then run the command:

````
  pod install
````

If IBDesigable is not rendering (affects some versions of Cocoapods), try adding this at the end of your podfile. 
[Here](https://github.com/CocoaPods/CocoaPods/issues/7606) is a relevant link on the subject. 

````
post_install do |installer|
    installer.pods_project.build_configurations.each do |config|
        config.build_settings.delete('CODE_SIGNING_ALLOWED')
        config.build_settings.delete('CODE_SIGNING_REQUIRED')
    end
end
````

# Manual

````
git clone https://gitlab.com/zakkhoyt/DurationSlider
````

* Drag and drop DurationSliderKit.xcodeproj into your xcode project
* Under your Project Settings -> General -> Embedded Binaries, click "+" then select DurationSliderKit
* Under your Project Settings -> Build Phases -> Target Dependencies, click "+" then select DurationSliderKit
* Build and run. 
