
Pod::Spec.new do |s|
  s.name         = "DurationSliderKit"
  s.version      = "1.0.0"
  s.summary      = "A timer duration picker similar to Apple's Bedtime tab from the Clock app."
  s.author        = { "Zakk Hoyt" => "vaporwarewolf@gmail.com" }
  s.homepage      = "https://gitlab.com/zakkhoyt/DurationSliderDemo"
  s.platforms = { :ios => 9.0
                }
  s.license = { :type => 'MIT',
                :text =>  <<-LICENSE
                  Copyright 2018. Zakk hoyt.
                          LICENSE
              }
  s.source       = { :git => 'https://gitlab.com/zakkhoyt/DurationSliderDemo.git',
                    :tag =>  "#{s.version}" }
  s.source_files  = 'DurationSliderKit/**/*.{swift}'
  s.requires_arc = true
  s.ios.deployment_target = '9.0'
end
