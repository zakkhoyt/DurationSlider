//
//  SnapTo.swift
//  DurationSliderDemo
//
//  Created by Zakk Hoyt on 7/24/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

enum SnapToMinutes: Int {
    case none
    case one
    case five
    case ten
    case fifteen
    case sixty
    
    static let count = 6
    static func from(seconds: TimeInterval) -> SnapToMinutes? {
        switch seconds {
        case 5: return .five
        case 1: return .one
        case 10: return .ten
        case 15: return .fifteen
        case 60: return .sixty
        default: return .none
        }
    }
    
    
    var description: String {
        switch self {
        case .none: return "Off"
        case .one: return "1"
        case .five: return "5"
        case .ten: return "10"
        case .fifteen: return "15"
        case .sixty: return "60"
        }
    }
    
    var seconds: TimeInterval? {
        switch self {
        case .none: return nil
        case .one: return 1 * TimeInterval.secondsPerMinute
        case .five: return 5 * TimeInterval.secondsPerMinute
        case .ten: return 10 * TimeInterval.secondsPerMinute
        case .fifteen: return 15 * TimeInterval.secondsPerMinute
        case .sixty: return 60 * TimeInterval.secondsPerMinute
        }
    }
}

enum SnapToSeconds: Int {
    case none
    case one
    case two
    case five
    case fifteen
    case sixty
    case threeHundred
    
    static let count = 7
    
    var description: String {
        switch self {
        case .none: return "Off"
        case .one: return "1"
        case .two: return "2"
        case .five: return "5"
        case .fifteen: return "15"
        case .sixty: return "60"
        case .threeHundred: return "300"
        }
    }
    
    var seconds: TimeInterval? {
        switch self {
        case .none: return nil
        case .one: return 1
        case .two: return 2
        case .five: return 5
        case .fifteen: return 15
        case .sixty: return 60
        case .threeHundred: return 300
        }
    }
}
