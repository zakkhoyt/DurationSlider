//
//  Theme.swift
//  DurationSliderDemo
//
//  Created by Zakk Hoyt on 7/24/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit
import DurationSliderKit

enum Theme: Int {
    case `default`
    case bedtime
    
    static let count = 2
    
    var description: String {
        switch self {
        case .default: return "Default"
        case .bedtime: return "Bedtime"
        }
    }
    
    var tintColor: UIColor {
        switch self {
        case .default: return .defaultTint
        case .bedtime: return .bedtimeYellow
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
        case .default: return .white
        case .bedtime: return .bedtimeBackground
        }
    }
    
    var trackColor: UIColor {
        switch self {
        case .default: return UIColor.lightGray.withAlphaComponent(0.25)
        case .bedtime: return .bedtimeTrack
        }
    }
    
    var accessoryLabelColor: UIColor {
        switch self {
        case .default: return .black
        case .bedtime: return .bedtimeDuration
        }
    }
    
    var durationTextAttributes: [NSAttributedString.Key: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        var attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 36, weight: UIFont.Weight.thin),
            .paragraphStyle: paragraphStyle
        ]
        
        switch self {
        case .default:
            attributes[.foregroundColor] = UIColor.darkGray
        case .bedtime:
            attributes[.foregroundColor] = UIColor.bedtimeDuration
        }
        return attributes
    }

    var clockTextAttributes: [NSAttributedString.Key: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        var attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 17),
            .paragraphStyle: paragraphStyle
        ]
        
        switch self {
        case .default:
            attributes[.foregroundColor] = UIColor.darkGray
        case .bedtime:
            attributes[.foregroundColor] = UIColor.bedtimeClock
        }
        return attributes
    }
}

extension UIColor {
    
    static let defaultTint = UIColor(hexString: "157EFB")
    
    static let bedtimeOrange = UIColor(hexString: "F99828")
    static let bedtimeYellow = UIColor(hexString: "F9C92B")
    static let bedtimeBackground = UIColor(hexString: "0C0D0E")
    static let bedtimeTrack = UIColor(hexString: "161718")
    static let bedtimeClock = UIColor(hexString: "8F9094")
    static let bedtimeDuration = UIColor(hexString: "FDFEFE")
    
    
    // MARK: Class factory
    
    /// Create a UIColor with a hex string
    ///
    /// - parameter hexString: RRGGBB or #RRGGBB where RR, GG, and BB range from "00" to "FF"
    /// - parameter alpha:     Alpha value from 0.0 to 1.0
    ///
    /// - returns: UIColor instance
    class func colorWith(hexString: String, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(hexString: hexString, alpha: alpha)
    }
    
    // MARK: Init methods
    
    /// Create a UIColor with a hex string
    ///
    /// - parameter hexString: RRGGBB or #RRGGBB
    /// - parameter alpha:     An alpha value from 0.0 to 1.0
    ///
    /// - returns: UIColor instance
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let scanner = Scanner(string: hexString)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
