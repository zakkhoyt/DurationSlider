//
//  DurationConfigurationViewController.swift
//  DurationSliderDemo
//
//  Created by Zakk Hoyt on 7/18/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit
import DurationSliderKit




class DurationConfigurationViewController: UIViewController {

    var durationSlider: DurationSlider!
    
    @IBOutlet weak var stepSegment: UISegmentedControl!
    @IBOutlet weak var maximumDurationSwitch: UISwitch!
    @IBOutlet weak var maximumDurationTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stepSegment.removeAllSegments()
        for i in 0..<SnapToMinutes.count {
            let step = SnapToMinutes(rawValue: i)!
            stepSegment.insertSegment(withTitle: step.description, at: i, animated: false)
        }
        if let snapToSeconds = durationSlider.snapToSeconds,
            let snapToMinute = SnapToMinutes.from(seconds: snapToSeconds) {
                stepSegment.selectedSegmentIndex = snapToMinute.rawValue
        } else {
            stepSegment.selectedSegmentIndex = -1
        }
        
        
        if let maximumDuration = durationSlider.maximumDuration {
            maximumDurationSwitch.isOn = true
            maximumDurationTextField.text = "\(Int(maximumDuration))"
        } else {
            maximumDurationSwitch.isOn = false
            maximumDurationTextField.text = ""
        }

    }
    
    @IBAction func stepSegmentValueChanged(_ sender: UISegmentedControl) {
        let step = SnapToMinutes(rawValue: sender.selectedSegmentIndex)!
        durationSlider.snapToSeconds = step.seconds
    }

    
    

}
