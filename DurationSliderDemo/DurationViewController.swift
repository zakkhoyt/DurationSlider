//
//  ViewController.swift
//  DurationSliderDemo
//
//  Created by Zakk Hoyt on 7/15/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit
import DurationSliderKit

class DurationViewController: UIViewController {
    
    // MARK: Internal vars
    
    @IBOutlet weak var durationSlider: DurationSlider!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var stepSegment: UISegmentedControl!
    @IBOutlet weak var themeSegment: UISegmentedControl!
    
    // MARK: Private vars
    
    private var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd\nhh:mm:ss a"
        return formatter
    }()
    
    // MARK: Override methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func setupThemeSegment() {
            themeSegment.removeAllSegments()
            for i in 0..<Theme.count {
                let theme = Theme(rawValue: i)!
                themeSegment.insertSegment(withTitle: theme.description, at: i, animated: false)
            }
            themeSegment.selectedSegmentIndex = Theme.default.rawValue
            themeSegmentValueChanged(themeSegment)
        }

        func setupStepSegment() {
            stepSegment.removeAllSegments()
            for i in 0..<SnapToMinutes.count {
                let step = SnapToMinutes(rawValue: i)!
                stepSegment.insertSegment(withTitle: step.description, at: i, animated: false)
            }
            stepSegment.selectedSegmentIndex = SnapToMinutes.fifteen.rawValue
            stepSegmentValueChanged(stepSegment)
        }
        
        func setupDurationSlider() {
            durationSlider.startKnobText = nil
            durationSlider.endKnobText = nil
            
            // Set startDate to next 15 minute division, endDate 4 hours later
            durationSlider.startDate = Date.nextInterval(seconds: 15 * TimeInterval.secondsPerMinute, date: Date())
            durationSlider.endDate = durationSlider.startDate.addingTimeInterval(TimeInterval.secondsPerHour * 4)
            
            durationSlider.minimumDuration = 30 * TimeInterval.secondsPerMinute
            durationSlider.maximumDuration = 18 * TimeInterval.secondsPerHour
            
            durationSlider.startKnobImage = UIImage(named: "start_24x24")
            durationSlider.endKnobImage = UIImage(named: "end_24x24")

            durationSliderValueChanged(durationSlider)
        }
        
        setupThemeSegment()
        setupStepSegment()
        setupDurationSlider()
    }
    
    // MARK: Private methods
    
    private func applyTheme(_ theme: Theme) {
        view.backgroundColor = theme.backgroundColor
        
        stepSegment.tintColor = theme.tintColor
        themeSegment.tintColor = theme.tintColor
        
        startLabel.textColor = theme.accessoryLabelColor
        endLabel.textColor = theme.accessoryLabelColor
        
        durationSlider.startKnobColor = theme.backgroundColor
        durationSlider.endKnobColor = theme.backgroundColor
        durationSlider.clockTextAttributes = theme.clockTextAttributes
        durationSlider.trackColor = theme.trackColor
        durationSlider.tintColor = theme.tintColor
        durationSlider.durationTextAttributes = theme.durationTextAttributes
    }

    // MARK: IBActions
    
    @IBAction func durationSliderValueChanged(_ sender: DurationSlider) {
        startLabel.text = "Start\n" + formatter.string(from: sender.startDate)
        endLabel.text = "End\n" + formatter.string(from: sender.endDate)
    }
    
    @IBAction func stepSegmentValueChanged(_ sender: UISegmentedControl) {
        let step = SnapToMinutes(rawValue: sender.selectedSegmentIndex)!
        durationSlider.snapToSeconds = step.seconds
    }
    
    @IBAction func themeSegmentValueChanged(_ sender: UISegmentedControl) {
        let theme = Theme(rawValue:  sender.selectedSegmentIndex)!
        applyTheme(theme)
    }
}

