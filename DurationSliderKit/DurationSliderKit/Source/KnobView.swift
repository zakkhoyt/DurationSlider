//
//  KnobView.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/3/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

open class KnobView: UIView {
    
    public var labelText: String? = nil {
        didSet {
            setNeedsLayout()
        }
    }
    
    public var borderWidth: CGFloat = 2 {
        didSet {
            setNeedsLayout()
        }
    }
    
    public var image: UIImage? = nil {
        didSet {
            setNeedsLayout()
        }
    }
    
    public var color: UIColor = .white {
        didSet {
            setNeedsLayout()
        }
    }
    open override var tintColor: UIColor! {
        didSet {
            setNeedsLayout()
        }
    }
    
    
    private var backgroundLayer = CAShapeLayer()
    private var knobLayer = CAShapeLayer()
    private var label: UILabel?
    private var imageView: UIImageView?

    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = .clear
        
        if backgroundLayer.superlayer == nil {
            layer.addSublayer(backgroundLayer)
        }
        backgroundLayer.fillColor = tintColor.cgColor
        backgroundLayer.path = UIBezierPath(arcCenter: centerPoint,
                                      radius: radius,
                                      startAngle: 0,
                                      endAngle: 2.0 * CGFloat.pi,
                                      clockwise: true).cgPath
        

        if knobLayer.superlayer == nil {
            layer.addSublayer(knobLayer)
        }
        knobLayer.fillColor = color.cgColor
        knobLayer.lineWidth = 0
        knobLayer.path = UIBezierPath(arcCenter: centerPoint,
                                        radius: radius - borderWidth,
                                        startAngle: 0,
                                        endAngle: 2.0 * CGFloat.pi,
                                        clockwise: true).cgPath


        
        
        if label == nil {
            let label = UILabel(frame: bounds)
            addSubview(label)
            label.textAlignment = .center
            label.text = labelText
            label.textColor = tintColor
            self.label = label
        }
        
        if let image = image {
            if imageView == nil {
                let imageView = UIImageView(frame: bounds)
                addSubview(imageView)
                imageView.contentMode = .center
                self.imageView = imageView
            }
            self.imageView?.image = image
        }
    }
}
