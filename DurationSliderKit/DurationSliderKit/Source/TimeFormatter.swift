//
//  DurationFormatter.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/3/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

struct TimeFormatter {
    
    func attributedDuration(_ duration: TimeInterval, attributes: [NSAttributedString.Key: Any], includeSeconds: Bool = false) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        let numberAttr = attributes
        var letterAttr = attributes
        
        // Try to make the letters half the height of the numbers
        if let font = letterAttr[NSAttributedString.Key.font] as? UIFont {
            let pointSize = font.pointSize
            let halfSize = pointSize / 2.0
            let halfFont = UIFont(name: font.fontName, size: halfSize)
            letterAttr[NSAttributedString.Key.font] = halfFont
        }
        
        let attrString = NSMutableAttributedString()
        let timeValues = timeValuesFrom(duration: duration)
        
        if timeValues.days > 0 {
            let daysString = String(format: "%d", timeValues.days)
            attrString.append(NSAttributedString(string: daysString, attributes: numberAttr))
            attrString.append(NSAttributedString(string: "DAY", attributes: letterAttr))
            attrString.append(NSAttributedString(string: " ", attributes: letterAttr))
        }
        
        if timeValues.hours > 0 {
            let hoursString = String(format: "%02d", Int(timeValues.hours))
            attrString.append(NSAttributedString(string: hoursString, attributes: numberAttr))
            attrString.append(NSAttributedString(string: "HR", attributes: letterAttr))
            attrString.append(NSAttributedString(string: " ", attributes: letterAttr))
        }
        
        let minutesString = String(format: "%02d", Int(timeValues.minutes))
        attrString.append(NSAttributedString(string: minutesString, attributes: numberAttr))
        attrString.append(NSAttributedString(string: "MIN", attributes: letterAttr))
        attrString.append(NSAttributedString(string: " ", attributes: letterAttr))
        
        if includeSeconds {
            let secondsString = String(format: "%02d", Int(timeValues.seconds))
            attrString.append(NSAttributedString(string: secondsString, attributes: numberAttr))
            attrString.append(NSAttributedString(string: "SEC", attributes: letterAttr))
        }
        return NSAttributedString(attributedString: attrString)
    }
    
    
    func attributedDuration(_ duration: TimeInterval, textColor: UIColor, includeSeconds: Bool = false) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        let numberAttr: [NSAttributedString.Key: Any] = [
            //.font: UIFont.systemFont(ofSize: 36),
            .font: UIFont.systemFont(ofSize: 20),
            .foregroundColor: textColor,
            .paragraphStyle: paragraphStyle
        ]
        let letterAttr: [NSAttributedString.Key: Any] = [
            //.font: UIFont.systemFont(ofSize: 18),
            .font: UIFont.systemFont(ofSize: 10),
            .foregroundColor: textColor,
            .paragraphStyle: paragraphStyle
        ]
        
        let attrString = NSMutableAttributedString()
        let timeValues = timeValuesFrom(duration: duration)
        
        if timeValues.days > 0 {
            let daysString = String(format: "%d", timeValues.days)
            attrString.append(NSAttributedString(string: daysString, attributes: numberAttr))
            attrString.append(NSAttributedString(string: "DAY", attributes: letterAttr))
            attrString.append(NSAttributedString(string: " ", attributes: letterAttr))
        }
        
        if timeValues.hours > 0 {
            let hoursString = String(format: "%02d", Int(timeValues.hours))
            attrString.append(NSAttributedString(string: hoursString, attributes: numberAttr))
            attrString.append(NSAttributedString(string: "HR", attributes: letterAttr))
            attrString.append(NSAttributedString(string: " ", attributes: letterAttr))
        }
        
        let minutesString = String(format: "%02d", Int(timeValues.minutes))
        attrString.append(NSAttributedString(string: minutesString, attributes: numberAttr))
        attrString.append(NSAttributedString(string: "MIN", attributes: letterAttr))
        attrString.append(NSAttributedString(string: " ", attributes: letterAttr))
        
        if includeSeconds {
            let secondsString = String(format: "%02d", Int(timeValues.seconds))
            attrString.append(NSAttributedString(string: secondsString, attributes: numberAttr))
            attrString.append(NSAttributedString(string: "SEC", attributes: letterAttr))
        }
        return NSAttributedString(attributedString: attrString)
    }

    private func timeValuesFrom(duration: TimeInterval) -> (days: Int, hours: Int, minutes: Int, seconds: Int) {
        var duration = duration
        
        let days = Int(duration) / Int(TimeInterval.secondsPerDay)
        duration -= (TimeInterval(days) * TimeInterval.secondsPerDay)
        
        let hours = Int(duration) / Int(TimeInterval.secondsPerHour)
        duration -= (TimeInterval(hours) * TimeInterval.secondsPerHour)
        
        let minutes = Int(duration) / Int(TimeInterval.secondsPerMinute)
        duration -= (TimeInterval(minutes) * TimeInterval.secondsPerMinute)
        
        let seconds = Int(duration)

        return (days, hours, minutes, seconds)
    }
}
