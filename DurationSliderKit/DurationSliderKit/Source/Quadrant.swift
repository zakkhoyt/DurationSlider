//
//  Quadrant.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/3/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

// This represents a Cartesian coordinate plane with the 4 quadrants that you learned in Algebra class.
enum Quadrant {
    case one
    case two
    case three
    case four
    
    var description: String {
        switch self {
        case .one: return "I"
        case .two: return "II"
        case .three: return "III"
        case .four: return "IV"
        }
    }
    static func quadrantFrom(point: CGPoint) -> Quadrant {
        if point.x >= 0 {
            if point.y >= 0 {
                return .one
            } else {
                return .four
            }
        } else {
            if point.y >= 0 {
                return .two
            } else {
                return .three
            }
        }
    }
}


