//
//  ClockLayer.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/2/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

class DurationClockView: UIView {
    
    // MARK: Internal vars

    var tickInset: CGFloat = 4 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var majorTickLength: CGFloat = 6 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var majorTickWidth: CGFloat = 2.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var minorTickLength: CGFloat = 4 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var minorTickWidth: CGFloat = 0.5 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var attributes: [NSAttributedString.Key: Any] = {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        return [
            .font: UIFont.systemFont(ofSize: 17),
            .foregroundColor: UIColor.darkGray,
            .paragraphStyle: paragraphStyle
        ]
        
        }() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // MARK: Private vars
    
    private var fontSize: CGFloat {
        guard let font = attributes[NSAttributedString.Key.font] as? UIFont else {
            return 17
        }
        return font.pointSize
    }
    
    private var textColor: UIColor {
        guard let foregroundColor = attributes[NSAttributedString.Key.foregroundColor] as? UIColor else {
            return .darkGray
        }
        return foregroundColor
    }

    // MARK: Override methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        context.setStrokeColor(textColor.cgColor)

        
        drawHourTicks(context: context)
        drawMinuteTicks(context: context)
        drawHourText(context: context)

    }

    // MARK: Private methods
    private func commonInit() {
        backgroundColor = UIColor.clear
    }

    private func drawHourTicks(context: CGContext) {
        let divisions = 12
        let anglePerDivision = 2.0 * CGFloat.pi / CGFloat(divisions)
        
        // Hour Ticks
        for i in 0..<divisions {
            context.setLineCap(.round)
            context.setLineWidth(majorTickWidth)
            
            let angle = CGFloat(i) * anglePerDivision
            let startRadius = radius - (tickInset)
            let endRadius = radius - (tickInset + majorTickLength)
            
            let startX = centerPoint.x + CGFloat(startRadius * sin(angle))
            let startY = centerPoint.y + CGFloat(startRadius * cos(angle))
            let startPoint = CGPoint(x: startX, y: startY)
            
            let endX = centerPoint.x + CGFloat(endRadius * sin(angle))
            let endY = centerPoint.y + CGFloat(endRadius * cos(angle))
            let endPoint = CGPoint(x: endX, y: endY)
            
            context.move(to: startPoint)
            context.addLine(to: endPoint)
        }
        context.strokePath()
    }
    
    private func drawMinuteTicks(context: CGContext) {
        let skip = 4
        let divisions = 12 * skip
        let anglePerDivision = 2.0 * CGFloat.pi / CGFloat(divisions)
        for i in 0..<divisions {
            
            if i % skip == 0 { continue }
            context.setLineCap(.round)
            context.setLineWidth(minorTickWidth)
            
            let angle = CGFloat(i) * anglePerDivision
            let startRadius = radius - (tickInset)
            let endRadius = radius - (tickInset + minorTickLength)
            
            let startX = centerPoint.x + CGFloat(startRadius * sin(angle))
            let startY = centerPoint.y + CGFloat(startRadius * cos(angle))
            let startPoint = CGPoint(x: startX, y: startY)
            
            let endX = centerPoint.x + CGFloat(endRadius * sin(angle))
            let endY = centerPoint.y + CGFloat(endRadius * cos(angle))
            let endPoint = CGPoint(x: endX, y: endY)
            
            context.move(to: startPoint)
            context.addLine(to: endPoint)
        }
        context.strokePath()
    }
    
    private func drawHourText(context: CGContext) {
        let divisions = 12
        let anglePerDivision = 2.0 * CGFloat.pi / CGFloat(divisions)
        for i in 0..<divisions {
            let text = (i == 0 ? "12" : "\(i)")
            let attrText = attributedText(for: text)
            let textRadius = radius - (tickInset + majorTickLength + fontSize)
            let angle = CGFloat.pi - CGFloat(i) * anglePerDivision
            let textX = centerPoint.x + CGFloat(textRadius * sin(angle))
            let textY = centerPoint.y + CGFloat(textRadius * cos(angle))
            let textPoint = CGPoint(x: textX, y: textY)
            drawText(attrText, at: textPoint)
        }
    }
    
    private func drawText(_ attributedText: NSAttributedString, at point: CGPoint) {
        let boundingRect = attributedText.boundingRect(with: bounds.size,
                                                       options: .usesLineFragmentOrigin,
                                                       context: nil)
        let centeredPoint = CGPoint(x: point.x - boundingRect.midX,
                                    y: point.y - boundingRect.midY)
        attributedText.draw(at: centeredPoint)
    }
    
    private func attributedText(for text: String) -> NSAttributedString {
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    
}
