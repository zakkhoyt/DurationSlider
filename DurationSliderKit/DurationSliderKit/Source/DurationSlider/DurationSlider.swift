//
//  DurationSlider.swift
//  DurationSliderDemo
//
//  Created by Zakk Hoyt on 7/2/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

@IBDesignable
open class DurationSlider: HapticControl {

    // MARK: Public vars
    
    /// The start date represented by the start knob
    open var startDate: Date = Date() {
        didSet {
            update()
            
            func startDateHaptic() {
                if let _ = snapToSeconds,
                    let object = touchHelper.touchObject,
                    object == .startKnob || object == .durationTrack,
                    let startDateHaptic = haptic.startDate,
                    startDateHaptic != startDate {
                    haptic.selectionChanged()
                }
                haptic.startDate = startDate
            }
            startDateHaptic()
        }
    }
    
    /// The end date represented by the end knob
    open var endDate: Date = Date().addingTimeInterval(6 * TimeInterval.secondsPerHour) {
        didSet {
            update()
            
            func endDateHaptic() {
                if let _ = snapToSeconds,
                    let object = touchHelper.touchObject,
                    object == .endKnob,
                    let endDateHaptic = haptic.endDate,
                    endDateHaptic != endDate {
                    haptic.selectionChanged()
                }
                haptic.endDate = endDate
            }
            endDateHaptic()
        }
    }
    
    /// Absolute minimum date
    open var minimumDate: Date? = nil
    
    /// Absolute maximum date
    open var maximumDate: Date? = nil

    /// The duration between startDate and endDate
    /// Value is read only
    open var duration: TimeInterval {
        return endDate.timeIntervalSince(startDate)
    }
    
    open var maximumDuration: TimeInterval? = nil {
        didSet {
            update()
        }
    }

    open var minimumDuration: TimeInterval? = nil {
        didSet {
            update()
        }
    }

    /// If set, the knobs will snap to nearest multiple of that value.
    /// For example if set to 5, knobs will snap to 1:00, 1:05, 1:10, etc...
    /// Value of nil is disabled.
    open var snapToSeconds: TimeInterval? = nil {
        didSet {
            update()
        }
    }
    
    @IBInspectable
    open var startKnobText: String? = "S" {
        didSet {
            update()
        }
    }
    
    @IBInspectable
    open var endKnobText: String? = "E" {
        didSet {
            update()
        }
    }
    
    /// Image to render inside the start knob.
    /// Will be rendered as a template image (tint color)
    /// Recommend a size of 24 points x 24 points if knobWidth
    /// is left at default value
    open var startKnobImage: UIImage? = nil {
        didSet {
            update()
        }
    }

    /// Image to render inside the end knob
    /// Will be rendered as a template image (tint color)
    /// Recommend a size of 24 points x 24 points if knobWidth
    /// is left at default value
    open var endKnobImage: UIImage? = nil {
        didSet {
            update()
        }
    }
    
    /// The central color of the start knob (not the borders)
    @IBInspectable
    open var startKnobColor: UIColor = .white {
        didSet {
            update()
        }
    }

    /// The central color of the end knob (not the border color)
    @IBInspectable
    open var endKnobColor: UIColor = .white {
        didSet {
            update()
        }
    }
    
    @IBInspectable
    open var trackColor: UIColor = UIColor.lightGray.withAlphaComponent(0.25) {
        didSet {
            update()
        }
    }

    @IBInspectable
    open var knobWidth: CGFloat = 44 {
        didSet {
            update()
        }
    }
    
    open override var tintColor: UIColor! {
        didSet {
            update()
        }
    }
    
    /// The duration text (in the center of the control) uses attributed
    /// strings to build the duration string. You can pass in what you like here.
    /// There will be an attempt to render the letters ("HR", "MIN", "DAY", etc...)
    /// in half the font size specified by this property.
    open var durationTextAttributes: [NSAttributedString.Key: Any] = {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        return [
            .font: UIFont.systemFont(ofSize: 20),
            .foregroundColor: UIColor.darkGray,
            .paragraphStyle: paragraphStyle
        ]
        
        }() {
        didSet {
            update()
        }
    }

    // MARK: Clock stuff
    
    @IBInspectable
    open var tickInset: CGFloat = 4 {
        didSet {
            clockView.tickInset = tickInset
        }
    }
    
    @IBInspectable
    open var majorTickLength: CGFloat = 6 {
        didSet {
            clockView.majorTickLength = majorTickLength
        }
    }
    
    @IBInspectable
    open var majorTickWidth: CGFloat = 2.0 {
        didSet {
            clockView.majorTickWidth = majorTickWidth
        }
    }
    
    @IBInspectable
    open var minorTickLength: CGFloat = 4 {
        didSet {
            clockView.minorTickLength = minorTickLength
        }
    }
    
    @IBInspectable
    open var minorTickWidth: CGFloat = 0.5 {
        didSet {
            clockView.minorTickWidth = minorTickWidth
        }
    }
    
    /// The duration text (in the center of the control) uses attributed
    /// strings to build the duration string. You can pass in what you like here.
    /// There will be an attempt to render the letters ("HR", "MIN", "DAY", etc...)
    /// in half the font size specified by this property.
    open var clockTextAttributes: [NSAttributedString.Key: Any] = {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        return [
            .font: UIFont.systemFont(ofSize: 17),
            .foregroundColor: UIColor.darkGray,
            .paragraphStyle: paragraphStyle
        ]
        
        }() {
        didSet {
            update()
        }
    }
    
    // MARK: Private vars
    
    private var durationLabel: UILabel!
    private var clockView: DurationClockView!
    private var startKnob: KnobView!
    private var endKnob: KnobView!
    private var trackLayer = CAShapeLayer()
    private var durationLayers: [CAShapeLayer] = []
    private var touchHelper = DurationTouchHelper()
    private var noonOffset: CGFloat = -CGFloat.pi / 2
    private var lastRevolutions: Int = 0
    private var insetRadius: CGFloat {
        return radius - knobWidth / 2.0
    }
    
    // MARK: Override methods
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        update()
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        touchHelper = DurationTouchHelper()
        touchHelper.startDate = startDate
        touchHelper.endDate = endDate
        let point = touch.location(in: self)
        touchHelper.centerPoint = centerPoint
        touchHelper.startPoint = point
        touchHelper.previousPoint = point
        
        if endKnob.frame.contains(point) {
            touchHelper.touchObject = .endKnob
        } else if startKnob.frame.contains(point) {
            touchHelper.touchObject = .startKnob
        } else if durationLayers(contains: point) {
            touchHelper.touchObject = .durationTrack
        } else {
            return
        }
        
        sendActions(for: .touchDown)
        processTracking(touch, with: event)
    }
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTracking(touch, with: event)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        processTracking(touch, with: event)
        sendActions(for: .touchUpInside)
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        sendActions(for: .touchCancel)
    }

    // MARK: Private methods
    
    private func processTracking(_ touch: UITouch, with event: UIEvent?) {
        guard let _ = touchHelper.startPoint else { return }
        guard let _ = touchHelper.centerPoint else { return }
        guard let touchObject = touchHelper.touchObject else { return }
        touchHelper.currentPoint = touch.location(in: self)
        guard let point = touchHelper.currentPoint else { return }
        guard var currentAngle = touchHelper.currentAngle else { return }

        // Examine if we should add or remove from the angle.
        // The math function atan2 breaks down when we cross the x axis where x < 0.
        // In other words when we go from quad 2 to 3, or quad 3 to 2.
        if let previousQuadrant = touchHelper.previousQuadrant,
            let currentQuadrant = touchHelper.currentQuadrant {
            if previousQuadrant == .two && currentQuadrant == .three {
                currentAngle += 2 * CGFloat.pi
            } else if previousQuadrant == .three && currentQuadrant == .two {
                currentAngle -= 2 * CGFloat.pi
            }
        }

        touchHelper.angleTotal += currentAngle
        
        let deltaTime = timeIntervalFor(angle: touchHelper.angleTotal)
        switch touchObject {
        case .startKnob:
            var startDate = round(date: touchHelper.startDate!.addingTimeInterval(deltaTime))
            startDate = clipDate(proposed: startDate)
            self.startDate = startDate
            
            if startDate >= endDate {
                endDate = startDate
            }
            if let maximumDuration = maximumDuration, endDate.timeIntervalSince(startDate) > maximumDuration {
                endDate = startDate.addingTimeInterval(maximumDuration)
            }
            if let minimumDuration = minimumDuration, endDate.timeIntervalSince(startDate) < minimumDuration {
                endDate = startDate.addingTimeInterval(minimumDuration)
            }
        case .endKnob:
            var endDate = round(date: touchHelper.endDate!.addingTimeInterval(deltaTime))
            endDate = clipDate(proposed: endDate)
            self.endDate = endDate
            
            if endDate <= startDate {
                startDate = endDate
            }
            if let maximumDuration = maximumDuration, endDate.timeIntervalSince(startDate) > maximumDuration {
                startDate = endDate.addingTimeInterval(-maximumDuration)
            }
            if let minimumDuration = minimumDuration, endDate.timeIntervalSince(startDate) < minimumDuration {
                startDate = endDate.addingTimeInterval(-minimumDuration)
            }
            
        case .durationTrack:
            var startDate = round(date: touchHelper.startDate!.addingTimeInterval(deltaTime))
            startDate = clipDate(proposed: startDate)
            self.startDate = startDate
            
            var endDate = round(date: touchHelper.endDate!.addingTimeInterval(deltaTime))
            endDate = clipDate(proposed: endDate)
            self.endDate = endDate
        }

        update()
        
        sendActions(for: .valueChanged)
        touchHelper.previousPoint = point
    }

    private func clipDate(proposed: Date) -> Date {
        return clipToMaximumDate(proposed: clipToMinimumDate(proposed: proposed))
    }
    
    private func clipToMinimumDate(proposed: Date) -> Date {
        guard let minimumDate = minimumDate else {
            return proposed
        }
        return max(proposed, minimumDate)
    }
    
    private func clipToMaximumDate(proposed: Date) -> Date {
        guard let maximumDate = maximumDate else {
            return proposed
        }
        return min(proposed, maximumDate)
    }
    
    private func round(date: Date) -> Date {
        guard let snapToSeconds = snapToSeconds else {
            return date
        }
        if snapToSeconds == 0 {
            return date
        }

        return Date.nearestInterval(seconds: snapToSeconds, date: date)
    }
    
    private func durationLayers(contains point: CGPoint) -> Bool {
        for durationLayer in durationLayers {
            if let path = durationLayer.path, path.contains(point) {
                return true
            }
        }
        return false
    }
    
    private func setup() {
        func setupLayers() {
            if trackLayer.superlayer == nil {
                layer.addSublayer(trackLayer)
            }
        }
        
        func setupClock() {
            
            if clockView == nil {
                clockView = DurationClockView(frame: .zero)
                addSubview(clockView)
            }
        }

        func setupKnobs() {
            let frame = CGRect(x: 0, y: 0, width: knobWidth, height: knobWidth)
            
            if startKnob == nil {
                startKnob = KnobView(frame: frame)
                startKnob.isUserInteractionEnabled = false
                addSubview(startKnob)
            }
    
            if endKnob == nil {
                endKnob = KnobView(frame: frame)
                endKnob.isUserInteractionEnabled = false
                addSubview(endKnob)
            }
        }
        
        func setupDurationLabel() {
            durationLabel = UILabel(frame: .zero)
            durationLabel.numberOfLines = 0
            addSubview(durationLabel)
            durationLabel.translatesAutoresizingMaskIntoConstraints = false
            durationLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            durationLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        }

        setupClock()
        setupLayers()
        setupKnobs()
        setupDurationLabel()
    }
    
    private func update() {
        func updateClockView() {
            clockView.frame = bounds.insetBy(dx: knobWidth, dy: knobWidth)
            clockView.attributes = clockTextAttributes
        }

        func updateKnobs() {
            let frame = CGRect(x: 0, y: 0, width: knobWidth, height: knobWidth)
            startKnob.frame = frame
            endKnob.frame = frame
            
            
            startKnob.color = startKnobColor
            endKnob.color = endKnobColor
            startKnob.tintColor = tintColor
            endKnob.tintColor = tintColor
            startKnob.labelText = startKnobText
            endKnob.labelText = endKnobText
            startKnob.center = pointForDate(startDate)
            endKnob.center = pointForDate(endDate)
            startKnob.image = startKnobImage?.imageWithColor(color: tintColor)
            endKnob.image = endKnobImage?.imageWithColor(color: tintColor)
        }

        func updateLayers() {
            trackLayer.fillColor = UIColor.clear.cgColor
            trackLayer.strokeColor = trackColor.cgColor
            trackLayer.lineWidth = knobWidth
            trackLayer.path = UIBezierPath(arcCenter: centerPoint,
                                           radius: insetRadius,
                                           startAngle: 0,
                                           endAngle: 2 * CGFloat.pi,
                                           clockwise: true).cgPath
            
            do {
                let revolutions = Int(floor(endDate.timeIntervalSince(startDate) / TimeInterval.secondsPerHalfDay) + 1)
                if revolutions < 0 {
                    return
                }

                let revDepth = knobWidth / CGFloat(revolutions)

                // Ensure that we have enough layers (one per ring/revolution)
                while durationLayers.count < revolutions {
                    let durationLayer = CAShapeLayer()
                    durationLayer.lineWidth = 1
                    layer.insertSublayer(durationLayer, above: trackLayer)
                    durationLayers.append(durationLayer)
                }

                // Erase unused layers (supposed we just went from 3 rings to 2. Erase the 3rd ring)
                for i in revolutions..<durationLayers.count {
                    let unusedLayer = durationLayers[i]
                    unusedLayer.path = UIBezierPath().cgPath
                }

                // Calculate starting index. There is no need to redraw the outer rings unless
                // the number of revolutions has changed
                let start = max(0, (lastRevolutions == revolutions) ? revolutions - 1 : 0)

                for r in start..<revolutions {
                    let startAngle = angleFor(date: startDate)
                    var endAngle: CGFloat
                    if r == revolutions - 1 {
                        endAngle = angleFor(date: endDate)
                        // Always draw in the correct direction
                        if startAngle - endAngle < 0 {
                            endAngle -= CGFloat.rotationsPerHalfDay
                        }
                    } else {
                        endAngle = startAngle + CGFloat.rotationsPerHalfDay
                    }

                    let innerRadius = CGFloat(r) * revDepth
                    let outerRadius = innerRadius + revDepth

                    let path = UIBezierPath()
                    path.addArc(withCenter: centerPoint,
                                radius: radius - knobWidth + innerRadius,
                                startAngle: startAngle,
                                endAngle: endAngle,
                                clockwise: true)

                    path.addArc(withCenter: centerPoint,
                                radius: radius - knobWidth + outerRadius,
                                startAngle: endAngle,
                                endAngle: startAngle,
                                clockwise: false)

                    let durationLayer = durationLayers[r]
                    durationLayer.fillColor = tintColor.cgColor
                    durationLayer.strokeColor = trackColor.cgColor
                    durationLayer.path = path.cgPath
                }
            }
        }
        
        func updateDurationLabel() {
            durationLabel.attributedText = TimeFormatter()
                .attributedDuration(duration, attributes: durationTextAttributes)
        }
        
        updateClockView()
        updateLayers()
        updateKnobs()
        updateDurationLabel()
    }
    
    private func pointForDate(_ date: Date) -> CGPoint {
        let angle = angleFor(date: date)
        let x: CGFloat = centerPoint.x + insetRadius * cos(angle)
        let y: CGFloat = centerPoint.y + insetRadius * sin(angle)
        return CGPoint(x: x, y: y)
    }

    private func angleFor(date: Date) -> CGFloat {
        let startOfDay = Calendar.current.startOfDay(for: date)
        let secondsSinceMidnight = date.timeIntervalSince(startOfDay)
        let factor = CGFloat(secondsSinceMidnight / TimeInterval.secondsPerDay)
        let rotationsPerDay = CGFloat.rotationsPerDay
        return rotationsPerDay * factor + noonOffset
    }
    
    private func timeIntervalFor(angle: CGFloat) -> TimeInterval {
        return TimeInterval(angle / CGFloat.rotationsPerDay) * TimeInterval.secondsPerDay
    }
}

