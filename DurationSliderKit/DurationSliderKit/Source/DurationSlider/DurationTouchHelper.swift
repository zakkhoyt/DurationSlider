//
//  TouchHelper.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/3/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

struct DurationTouchHelper {
    
    enum TouchObject {
        case startKnob
        case endKnob
        case durationTrack
        
        var description: String {
            switch self {
            case .startKnob: return "StartKnob"
            case .endKnob: return "EndKnob"
            case .durationTrack: return "DurationTrack"
            }
        }
    }

    var startPoint: CGPoint?
    var currentPoint: CGPoint?
    var previousPoint: CGPoint?
    
    var centerPoint: CGPoint?
    var touchObject: TouchObject?
    
    var startDate: Date?
    var endDate: Date?
    
    var currentAngle: CGFloat? {
        guard let previousPoint = previousPoint else { return nil }
        guard let endPoint = currentPoint else { return nil }
        guard let centerPoint = centerPoint else { return nil }
        func angleBetween(centerPoint: CGPoint, startPoint: CGPoint, endPoint: CGPoint) -> CGFloat {
            return atan2(endPoint.y - centerPoint.y, endPoint.x - centerPoint.x) -
                atan2(startPoint.y - centerPoint.y, startPoint.x - centerPoint.x)
        }
        
        return angleBetween(centerPoint: centerPoint, startPoint: previousPoint, endPoint: endPoint)
    }
    
    var angleTotal: CGFloat = 0
    
    
    
    var startQuadrant: Quadrant? {
        guard let startPoint = startPoint else { return nil }
        guard let centerPoint = centerPoint else { return nil }
        let x = startPoint.x - centerPoint.x
        let y = startPoint.y - centerPoint.y
        let point = CGPoint(x: x, y: y)
        return Quadrant.quadrantFrom(point: point)
    }
    
    var currentQuadrant: Quadrant? {
        guard let endPoint = currentPoint else { return nil }
        guard let centerPoint = centerPoint else { return nil }
        let x = endPoint.x - centerPoint.x
        let y = endPoint.y - centerPoint.y
        let point = CGPoint(x: x, y: y)
        return Quadrant.quadrantFrom(point: point)
    }
    
    var previousQuadrant: Quadrant? {
        guard let previousPoint = previousPoint else { return nil }
        guard let centerPoint = centerPoint else { return nil }
        let x = previousPoint.x - centerPoint.x
        let y = previousPoint.y - centerPoint.y
        let point = CGPoint(x: x, y: y)
        return Quadrant.quadrantFrom(point: point)
    }


    var description: String {
        guard let startPoint = startPoint else {
            return "No startPoint"
        }
        guard let endPoint = currentPoint else {
            return "No endPoint"
        }
//        guard let angle = angle else {
//            return "No angle"
//        }
        guard let currentAngle = currentAngle else {
            return "No currentAngle"
        }

        guard let touchObject = touchObject else {
            return "No touchObject"
        }
        return "\(touchObject.description) start: \(startPoint) end: \(endPoint) currentAngle: \(currentAngle) angleTotal: \(angleTotal)"
    }
}
