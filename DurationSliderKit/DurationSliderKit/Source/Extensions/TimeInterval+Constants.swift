//
//  TimeInterval+Constants.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/3/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

public extension TimeInterval {
    static let secondsPerDay: TimeInterval = 24 * 60 * 60       // 86400
    static let secondsPerHalfDay: TimeInterval = 12 * 60 * 60   // 43200
    static let secondsPerHour: TimeInterval = 60 * 60           // 3600
    static let secondsPerMinute: TimeInterval = 60              // 60
    
    
    static func nearestInterval(seconds: TimeInterval, duration: TimeInterval) -> TimeInterval {
        let divisor = seconds
        var dividend = duration / divisor
        dividend = dividend.rounded()
        dividend *= divisor
        return dividend
    }

    
}


