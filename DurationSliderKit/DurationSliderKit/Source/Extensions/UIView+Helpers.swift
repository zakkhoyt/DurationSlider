//
//  UIView+Helpers.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/3/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

extension UIView {
    var centerPoint: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    var radius: CGFloat {
        return min(bounds.width, bounds.height) / 2.0
    }
}
