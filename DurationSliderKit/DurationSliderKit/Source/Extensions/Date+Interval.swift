//
//  Date+Interval.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/16/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

public extension Date {
    /// Returns a date which is aligned to the next interval.
    /// For example if it is 1:13PM and we call this function with
    /// seconds: (5 * 60), then it will retur 1:15PM
    ///
    /// - Parameter seconds: Number of seconds to snap to
    /// - Returns: Date which is aligned to seconds
    static func nextInterval(seconds: TimeInterval, date: Date) -> Date {
        let startOfDay = Calendar.current.startOfDay(for: date)
        let delta = date.timeIntervalSince(startOfDay)
        let divisor = seconds
        
        var dividend = delta / divisor
        dividend = floor(dividend) + 1.0
        dividend *= divisor
        return startOfDay.addingTimeInterval(dividend)
    }
    
    /// Returns a date which is aligned to the next interval.
    /// For example if it is 1:13PM and we call this function with
    /// seconds: (5 * 60), then it will retur 1:15PM
    ///
    /// - Parameter seconds: Number of seconds to snap to
    /// - Returns: Date which is aligned to seconds
    static func nearestInterval(seconds: TimeInterval, date: Date) -> Date {
        let startOfDay = Calendar.current.startOfDay(for: date)
        let delta = date.timeIntervalSince(startOfDay)
        let divisor = seconds
        
        var dividend = delta / divisor
        dividend = dividend.rounded()
        dividend *= divisor
        return startOfDay.addingTimeInterval(dividend)
    }
}
