//
//  UIColor+random.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/14/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

public extension UIColor {
    static func random(minimumComponentValue base: CGFloat = 0.5,
                alpha: CGFloat = 1.0) -> UIColor {
        let seed = 1.0 - base
        let red = base + CGFloat.random(seed: seed)
        let green = base + CGFloat.random(seed: seed)
        let blue = base + CGFloat.random(seed: seed)
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
}



