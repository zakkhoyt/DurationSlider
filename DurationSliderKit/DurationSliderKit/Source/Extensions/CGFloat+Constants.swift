//
//  CGFloat+Constants.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/11/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

internal extension CGFloat {
    static let rotationsPerDay = 4 * CGFloat.pi     // 12.566370614359173
    static let rotationsPerHalfDay = 2 * CGFloat.pi // 6.283185307179586
    
    static let rotationsPerMinute = 2 * CGFloat.pi // 6.283185307179586
    static let rotationsPerHour = 2 * CGFloat.pi // 6.283185307179586
}

public extension CGFloat {
    static func random(seed: CGFloat) -> CGFloat {
        return seed * CGFloat(arc4random()) / CGFloat(UINT32_MAX)
    }
}
