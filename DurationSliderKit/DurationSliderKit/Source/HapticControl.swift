//
//  HapticControl.swift
//  DurationSliderKit
//
//  Created by Zakk Hoyt on 7/25/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

open class HapticControl: UIControl {
    class Haptic {
        var startDate: Date?
        var endDate: Date?
        var duration: TimeInterval?
        
        private var selectionWrapper: Any?
        
        init() {
            // TODO: This is a really lousy way of supporting haptic and iOS 9
            // We are stuffing UISelectionFeedbackGenerator into an Any var
            // and trying to unwrap it to use it.
            // There has to be a better way. Currenltly having a brain fart.
            if #available(iOS 10.0, *) {
                let feedback = UISelectionFeedbackGenerator()
                feedback.prepare()
                selectionWrapper = feedback as Any
            }
        }
        
        func selectionChanged() {
            if #available(iOS 10.0, *) {
                if let selection = selectionWrapper as? UISelectionFeedbackGenerator {
                    selection.selectionChanged()
                }
            } else {
                //print("iOS too old for haptics")
            }
        }
    }

    
    let haptic = Haptic()
}

